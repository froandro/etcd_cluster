#! /usr/bin/env bash

##### Install Etcd #####
echo "-------- Install Etcd --------"
sudo apt update
sudo apt install etcd -y

##### Config Etcd #####
echo "------ Config Etcd --------"

cat > /etc/default/etcd <<EOF
enable-v2: true
name: '$name'
listen-peer-urls: 'http://$ip_etcd:2380'
listen-client-urls: 'http://$ip_etcd:2379, http://127.0.0.1:2379'
initial-advertise-peer-urls: 'http://$ip_etcd:2380'
advertise-client-urls: 'http://$ip_etcd:2379'

initial-cluster: 'etcd_1=http://$ip_etcd1:2380,etcd_2=http://$ip_etcd2:2380,etcd_3=http://$ip_etcd3:2380'
initial-cluster-state: 'new'
initial-cluster-token: 'cluster_etcd'
EOF

pgrep etcd | xargs kill
nohup etcd --config-file /etc/default/etcd &
