#! /usr/bin/env bash

##### Install Etcd #####
echo "-------- Install Etcd --------"
apt update
apt install etcd -y

"##### Config Etcd #####
echo "------ Config Etcd --------
etcd --name eettccdd --listen-client-urls=http://0.0.0.0:2379 --advertise-client-urls=http://0.0.0.0:2379
UUID=$(uuidgen)
echo $UUID
curl -X PUT http://$ip_etcd/v2/keys/_etcd/registry/$UUID/_config/size -d value=$cluster_size/
#curl -X PUT http://$ip_etcd/v2/keys/_etcd/registry/$UUID/${member_id}?prevExist=false -d value="${member_name}=${member_peer_url_1}&${member_name}=${member_peer_url_2}"




#curl -X PUT http://50.50.50.1:2379/v2/keys/_etcd/registry/${UUID}/_config/size -d value=3



#cat > /etc/default/etcd <<EOF
#ETCD_LISTEN_PEER_URLS="http://$ip_etcd:2380"
#
#ETCD_LISTEN_CLIENT_URLS="http://localhost:2379,http://$ip_etcd:2379"
#
#ETCD_INITIAL_ADVERTISE_PEER_URLS="http://$ip_etcd:2380"
#
#ETCD_INITIAL_CLUSTER="etcd0=http://$ip_etcd:2380,"
#
#ETCD_ADVERTISE_CLIENT_URLS="http://$ip_etcd:2379"
#
#ETCD_INITIAL_CLUSTER_TOKEN="cluster_etcd"
#
#ETCD_INITIAL_CLUSTER_STATE="new"
#
#EOF

sudo systemctl restart etcd